

nums = [9,6,4,2,3,5,7,0,1]

def findMissingNumber(array):
    array.sort()
    print(f"Sorted Array: {array}\n")
    missingNumbers = []
    itr = array[0]

    for nums in range(0,len(array)):
        print(f"Array: {array[nums]} itr: {itr}\n")
        if itr != array[nums]:
            missingNumbers.append(itr)
        itr = itr + 1
    return missingNumbers

print(f"Numbers missing fom the Array{findMissingNumber(nums)}")
