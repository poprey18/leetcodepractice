

array = [2,2,1,1,1,2,2]

def findMajorityElement(nums):
    counter, majorityElement = 0,0
    for num in range(0,len(nums)):
        if nums.count(nums[num]) > counter :
            counter = nums.count(nums[num])
            majorityElement = nums[num]
    return majorityElement

print(f"Majority Element is: {findMajorityElement(array)}")

