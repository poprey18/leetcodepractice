
palindrome = "Do geese see God"

def cleanStr(str):
    print(f"Original: {str}")
    str = str.lower()
    str = str.replace(" ","")
    print(f"Cleaned: {str}")
    return str


#Solution 1
def checkPalindrome(pal):
    palReversed = pal[::-1]
    if pal == palReversed:
        return True
    else: 
        return False

#Solution 2
# def checkPalindrome(pal):
#     pal.lower()
#     pal.strip()
#     palLength = len(palindrome)
#     palLength = palLength -1
#     print(f"String Length: {palLength}")
#     if (palLength % 2) == 0:
#         print("Even")
#     else:
#         print("odd")

#     counter = -1
#     for leters in range(0, len(pal)):
#         print(f"{pal[leters]} : {pal[counter]} \n")
#         if pal[leters] != pal[counter]:
#             return False
#         counter = counter -1
#     return True

# print(f"Is {palindrome} a Palindrome: {checkPalindrome(cleanStr(palindrome))}")

print(f"Is '{palindrome}' a Palindrome: {checkPalindrome(cleanStr(palindrome))}")