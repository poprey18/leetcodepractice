class Node:
    def __init__(self, val=0, next=None, pos=0):
        self.val = val
        self.next = next
        self.pos = pos

class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, val):
        if self.head:
            current = self.head
            while current.next:
                current = current.next
            newPos = current.pos + 1
            current.next = Node(val, None, newPos)
        else:
            self.head = Node(val, None, 0)
    
    def pintLL(self):
        current = self.head
        while(current):
            print(f"Node Value:{current.val} Node Position:{current.pos}")
            current = current.next

def mergeTwoSortedLists(l1=None, l2=None):
    sortedList = []
    while l1.head or l2.head:
        if l1.head.val < l2.head.val:
            sortedList.append(l1.head.val)
            sortedList.append(l2.head.val)
            l1.head = l1.head.next
            l2.head = l2.head.next
        else:
            sortedList.append(l2.head.val)
            sortedList.append(l1.head.val)
            l1.head = l1.head.next
            l2.head = l2.head.next
    return sortedList

li0 = LinkedList()
li0.insert(1)
li0.insert(2)

li1 = LinkedList()
li1.insert(1)
li1.insert(3)
li1.insert(4)

print(mergeTwoSortedLists(li0, li1))
