# Creates a set from the given array. sets cannot have 2 of the same value
# so duplicated values cannot be added to the set. we then compare the length of the
# set and array. if the set is smaller, a value must be a duplicate

nums = [1,2,3,1]

def checkDuplicate(array):
    x = set(array)
    if len(x) == len(array):
        return False
    else: return True

print(f"Does the array contain Duplicate entries: {checkDuplicate(nums)}")
